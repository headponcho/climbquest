﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField] float verticalOffset = 2.0f;

    public Transform player;

    Vector3 leftBorder;
    Vector3 rightBorder;
    float worldCameraWidth;

    Vector3 cameraFreezePos;
    bool isCameraFrozen = false;

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        if (isCameraFrozen) {
            Camera.main.transform.position = cameraFreezePos;
        } else if (player) {
            Camera.main.transform.position = new Vector3(player.position.x, player.position.y + verticalOffset, transform.position.z);

            leftBorder.y = transform.position.y;
            rightBorder.y = transform.position.y;

            Vector3 leftBorderViewportPos = Camera.main.WorldToViewportPoint(leftBorder);
            Vector3 rightBorderViewportPos = Camera.main.WorldToViewportPoint(rightBorder);

            // distance to move camera so left & right sides do not pass the world border
            float cameraOffset = 0.0f;

            // if the camera's left edge has passed the left world border
            if (leftBorderViewportPos.x > 0.0f) {
                cameraOffset = leftBorderViewportPos.x * worldCameraWidth;
            // if the camera's right edge has passed the right world border
            } else if (rightBorderViewportPos.x < 1.0f) {
                cameraOffset = (rightBorderViewportPos.x - 1.0f) * worldCameraWidth;
            }

            Camera.main.transform.position = new Vector3(transform.position.x + cameraOffset, transform.position.y, transform.position.z);
        }
    }

    public void UpdateWorldBorders(GameObject leftBorderObject, GameObject rightBorderObject) {
        leftBorder = leftBorderObject.transform.position;
        leftBorder.x += leftBorderObject.GetComponent<BoxCollider>().bounds.extents.x;
        rightBorder = rightBorderObject.transform.position;
        rightBorder.x -= rightBorderObject.GetComponent<BoxCollider>().bounds.extents.x;

        float zUnitsFromCamera = leftBorder.z - transform.position.z;

        // Width covered by the camera in world units on the Z-plane of the borders
        worldCameraWidth = Camera.main.ViewportToWorldPoint(new Vector3(1.0f, 0.0f, zUnitsFromCamera)).x - 
                           Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, zUnitsFromCamera)).x;
    }

    public void FreezeCamera() {
        isCameraFrozen = true;
        cameraFreezePos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }

    public void UnfreezeCamera() {
        isCameraFrozen = false;
    }
}
