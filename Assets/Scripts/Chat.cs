﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Chat : MonoBehaviour {

    protected const short chatMessageId = 7943;

    public virtual void SendChatMessage() { }

    protected virtual void ReceieveMessage(NetworkMessage message) { }

    protected string Bold(string text) {
        return "<b>" + text + "</b>";
    }

    protected string Italic(string text) {
        return "<i>" + text + "</i>";
    }

    protected string BoldItalic(string text) {
        return Bold(Italic(text));
    }
}
