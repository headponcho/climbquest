﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class ChatClient : Chat {

    [SerializeField] private Text chatHistory;
    [SerializeField] private ScrollRect scrollRect;
    [SerializeField] private GameObject chatInput;
    private InputField chatInputField;

    void Start() {
        if (NetworkClient.active) {
            NetworkManager.singleton.client.RegisterHandler(chatMessageId, ReceieveMessage);
            scrollRect.verticalNormalizedPosition = 0;
            chatInputField = chatInput.GetComponentInChildren<InputField>();
        }
        DisableChatInput();
    }

    public bool IsChatInputEnabled() {
        return chatInput.activeSelf;
    }

    public void EnableChatInput() {
        chatInput.SetActive(true);
        chatInputField.ActivateInputField();
        chatInputField.Select();
    }

    public void DisableChatInput() {
        chatInput.SetActive(false);
    }

    protected override void ReceieveMessage(NetworkMessage message) {
        string text = message.ReadMessage<StringMessage>().value;
        AddMessage(text);
    }

    private void AddMessage(string message) {
        chatHistory.text += "\n" + message;
        scrollRect.verticalNormalizedPosition = 0;
    }

    public override void SendChatMessage() {
        StringMessage message = new StringMessage();
        message.value = chatInputField.text;

        chatInputField.text = "";

        if (NetworkClient.active && message.value != "") {
            NetworkManager.singleton.client.Send(chatMessageId, message);
        }

        DisableChatInput();
    }
}
