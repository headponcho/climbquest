﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.UI;

public class ChatServer : Chat {

    [SerializeField] Color serverMessageColor = Color.yellow;

    void Start() {
        if (NetworkServer.active) {
            NetworkServer.RegisterHandler(chatMessageId, ReceieveMessage);
        }       
    }

    protected override void ReceieveMessage(NetworkMessage message) {
        StringMessage chatMessage = new StringMessage();
        chatMessage.value = BoldItalic(ClimbQuestNetworkManager.playerList[message.conn.connectionId].playerName) + 
                            ": " + 
                            message.ReadMessage<StringMessage>().value;

        NetworkServer.SendToAll(chatMessageId, chatMessage);
    }

    public void SendPlayerConnectedMessage(string playerName) {
        SendPlayerMessage(playerName, "has joined the game.");
    }

    public void SendPlayerDisconnectedMessage(string playerName) {
        SendPlayerMessage(playerName, "has left the game.");
    }

    public void SendPlayerVictoryMessage(string playerName) {
        SendPlayerMessage(playerName, "has reached the top!");
    }

    private void SendPlayerMessage(string playerName, string message) {
        StringMessage chatMessage = new StringMessage();
        chatMessage.value = BoldItalic(playerName) + " " + message;

        ApplyServerMessageColor(chatMessage);

        NetworkServer.SendToAll(chatMessageId, chatMessage);
    }

    private void ApplyServerMessageColor(StringMessage message) {
        message.value = "<color=#" + ColorUtility.ToHtmlStringRGBA(serverMessageColor) + ">" + message.value + "</color>";
    }
}
