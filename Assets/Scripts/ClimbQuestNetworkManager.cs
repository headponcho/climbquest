﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class ClimbQuestNetworkManager : NetworkManager {

    public static Dictionary<int, PlayerController> playerList = new Dictionary<int, PlayerController>();

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId) {
        Transform startPos = GetStartPosition();
        GameObject localPlayer = (GameObject)GameObject.Instantiate(playerPrefab, startPos.position, startPos.rotation);
        NetworkServer.AddPlayerForConnection(conn, localPlayer, playerControllerId);
        PlayerController localPlayerController = localPlayer.GetComponent<PlayerController>();
        playerList.Add(conn.connectionId, localPlayerController);
        localPlayerController.RpcInitialLevelTransition();
    }

    public override void OnServerDisconnect(NetworkConnection conn) {
        int connId = conn.connectionId;
        ChatServer chat = FindObjectOfType<ChatServer>();
        if (chat != null) {
            chat.SendPlayerDisconnectedMessage(playerList[connId].playerName);
        }
        if (playerList.ContainsKey(connId)) {
            playerList.Remove(connId);
        }
        base.OnServerDisconnect(conn);
    }
}
