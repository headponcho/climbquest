﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Eagle : NetworkBehaviour {

    [SerializeField] float speed = 5.0f;
    [SerializeField] float timeBeforeReturn = 1.0f;
    [SerializeField] AudioClip spawnSound;
    private Nest origin;
    private GameObject target;
    private Rigidbody rbody;
    private GameObject playerReached;

	// Use this for initialization
	void Start () {
        rbody = GetComponent<Rigidbody>();
        GetComponent<AudioSource>().PlayOneShot(spawnSound);
    }
	
	void FixedUpdate () {
        if (target != null) {
            Vector3 direction = target.transform.position - rbody.position;
            direction.Normalize();
            rbody.MovePosition(rbody.position + (direction * speed * Time.fixedDeltaTime));

            if (playerReached != null) {
                playerReached.transform.position = transform.position;
            }
        }
	}

    private void OnTriggerEnter(Collider other) {
        PlayerController player = other.gameObject.GetComponent<PlayerController>();
        if (player != null && player.gameObject == target) {
            playerReached = player.gameObject;
            player.SetPlayerControllable(false);
            SetCurrentTarget(origin.eagleDestination);
            StartCoroutine(ReturnPlayer());
        }
    }

    IEnumerator ReturnPlayer() {
        yield return new WaitForSeconds(timeBeforeReturn);
        if (NetworkServer.active) {
            playerReached.GetComponent<PlayerController>().RpcTransitionToLevel(origin.returnTo.netId);
        } else {
            playerReached.GetComponent<PlayerController>().TransitionToLevel(origin.returnTo);
        }
        origin.PlayerReturned(playerReached);
        playerReached.GetComponent<PlayerController>().SetPlayerControllable(true);
        Destroy(gameObject);
    }

    public void SetCurrentTarget(GameObject target) {
        this.target = target;
        transform.forward = (target.transform.position - transform.position).normalized;
    }

    public void SetOrigin(Nest origin) {
        this.origin = origin;
    }

    [ClientRpc]
    public void RpcSetCurrentTarget(NetworkInstanceId targetId) {
        SetCurrentTarget(ClientScene.FindLocalObject(targetId));
    }

    [ClientRpc]
    public void RpcSetOrigin(NetworkInstanceId originId) {
        SetOrigin(ClientScene.FindLocalObject(originId).GetComponent<Nest>());
    }
}
