﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Goal : MonoBehaviour {

    [SerializeField] LevelTransition nextLevel;
    [SerializeField] AudioClip win;
    AudioSource audioSource;
    List<int> transitioningPlayers = new List<int>();

    private void Start() {
        audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other) {
        PlayerController player = other.gameObject.GetComponent<PlayerController>();
        NetworkConnection playerConn = player.connectionToClient;
        int playerId = -1;
        if (playerConn != null) {
            playerId = playerConn.connectionId;
        }
        if (player != null && !transitioningPlayers.Contains(playerId)) {
            StartCoroutine(SendPlayerToNextLevel(player.gameObject, playerId));

            ChatServer chatServer = FindObjectOfType<ChatServer>();
            if (chatServer != null) {
                chatServer.SendPlayerVictoryMessage(player.playerName);
            }
        }
    }

    IEnumerator SendPlayerToNextLevel(GameObject player, int playerId) {
        audioSource.PlayOneShot(win);
        transitioningPlayers.Add(playerId);
        yield return new WaitForSeconds(1.5f);
        player.GetComponent<PlayerController>().TransitionToLevel(nextLevel);
        transitioningPlayers.Remove(playerId);
    }
}
