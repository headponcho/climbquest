﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class HostMenu : MonoBehaviour {

    [SerializeField] GameObject hostMenuItems;
    [SerializeField] GameObject portInput;
    [SerializeField] GameObject playerNameInput;

    public void Host(float timeBeforeLoad) {
        int port;
        bool isValidPortInput = int.TryParse(portInput.GetComponent<InputField>().text, out port);
        string playerName = playerNameInput.GetComponent<InputField>().text;

        if (!isValidPortInput) {
            port = 7777;
        }

        if (string.IsNullOrEmpty(playerName)) {
            playerName = "Player";
        }

        FindObjectOfType<PlayerConfig>().playerName = playerName;

        Debug.Log("Starting server on " + ClimbQuestNetworkManager.singleton.networkAddress + ":" + ClimbQuestNetworkManager.singleton.networkPort);
        hostMenuItems.SetActive(false);
        StartCoroutine(SpawnEagle(timeBeforeLoad));
        StartCoroutine(StartServer(timeBeforeLoad + 0.7f));
    }

    IEnumerator SpawnEagle(float timeBeforeLoad) {
        yield return new WaitForSeconds(timeBeforeLoad);
        FindObjectOfType<Nest>().SpawnEagle(FindObjectOfType<PlayerController>().gameObject);
    }

    IEnumerator StartServer(float timeBeforeLoad) {
        yield return new WaitForSeconds(timeBeforeLoad);
        ClimbQuestNetworkManager.singleton.StartHost();
    }
}
