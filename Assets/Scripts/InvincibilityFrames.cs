﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibilityFrames : MonoBehaviour {

    [Tooltip("Time before player can get hit again in seconds")]
    [SerializeField] float invincibilityTime = 1.0f;
    [Tooltip("Time between flicker toggles in seconds (lower = faster flicker)")]
    [SerializeField] float flickerRate = 0.05f;

    float elapsedTime = 0.0f;
    bool isInvincible = false;
    MeshRenderer[] meshRenderers;
    SkinnedMeshRenderer[] skinnedMeshRenderers;

    void Start() {
        meshRenderers = GetComponentsInChildren<MeshRenderer>();
        skinnedMeshRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();
    }

    void Update() {
        if (isInvincible) {
            elapsedTime += Time.deltaTime;

            if (elapsedTime > flickerRate) {
                elapsedTime -= flickerRate;

                ToggleRenderers();
            }
        }
    }

    private void ToggleRenderers() {
        SetRenderers(!meshRenderers[0].enabled);
    }

    private void SetRenderers(bool enabled) {
        foreach (MeshRenderer renderer in meshRenderers) {
            renderer.enabled = enabled;
        }
        foreach (SkinnedMeshRenderer renderer in skinnedMeshRenderers) {
            renderer.enabled = enabled;
        }
    }

    public bool IsInvincible() {
        return isInvincible;
    }

    public void OnHit() {
        isInvincible = true;
        elapsedTime = 0.0f;
        StartCoroutine(StopInvincibility(invincibilityTime));
    }

    IEnumerator StopInvincibility(float invincibilityTime) {
        yield return new WaitForSeconds(invincibilityTime);
        isInvincible = false;
        SetRenderers(true);
    }
}
