﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class JoinMenu : MonoBehaviour {

    [SerializeField] GameObject joinMenuItems;
    [SerializeField] GameObject backButton;
    [SerializeField] GameObject ipAddressInput;
    [SerializeField] GameObject portInput;
    [SerializeField] GameObject playerNameInput;

    public void Connect(float timeBeforeLoad) {
        string ip = ipAddressInput.GetComponent<InputField>().text;
        int port;
        bool isValidPortInput = int.TryParse(portInput.GetComponent<InputField>().text, out port);
        string playerName = playerNameInput.GetComponent<InputField>().text;

        if (string.IsNullOrEmpty(ip)) {
            ip = "localhost";
        }

        if (!isValidPortInput) {
            port = 7777;
        }

        if (string.IsNullOrEmpty(playerName)) {
            playerName = "Player";
        }

        FindObjectOfType<PlayerConfig>().playerName = playerName;

        ClimbQuestNetworkManager.singleton.networkAddress = ip;
        Debug.Log("Connecting to " + ClimbQuestNetworkManager.singleton.networkAddress + ":" + ClimbQuestNetworkManager.singleton.networkPort);
        ClimbQuestNetworkManager.singleton.StartClient();
    }
}
