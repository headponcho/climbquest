﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class KnockbackForce : NetworkBehaviour {

    [SerializeField] [SyncVar] float force = 20.0f;
    [Tooltip("If enabled, the knockback direction will be based on the Rigidbody's velocity. " + 
             "Otherwise, the direction will be the opposite of the player's direction. ")]
    [SerializeField] bool useVelocity;

    void OnTriggerEnter(Collider other) {
        PlayerController player = other.gameObject.GetComponent<PlayerController>();
        if (player != null) {
            if (!player.GetComponent<InvincibilityFrames>().IsInvincible()) {
                player.GetComponent<InvincibilityFrames>().OnHit();

                Vector3 direction;
                if (useVelocity) {
                    direction = gameObject.GetComponent<Rigidbody>().velocity.normalized;
                } else {
                    direction = player.moveDirection * -1.0f;
                }

                player.GetComponent<Rigidbody>().AddForce(direction * force, ForceMode.VelocityChange);
                player.OnDamage();
            }
        }
    }

    public void SetForce(float force) {
        this.force = force;
    }
}
