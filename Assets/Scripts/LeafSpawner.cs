﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeafSpawner : MonoBehaviour {

    ParticleSystem leaves;

    void Start() {
        leaves = GetComponent<ParticleSystem>();
    }

    private void OnTriggerEnter(Collider other) {
        Projectile projectile = other.GetComponent<Projectile>();
        if (projectile != null) {
            leaves.Play();
        }
    }
}
