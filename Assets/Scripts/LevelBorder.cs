﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBorder : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Destroy(gameObject.GetComponent<MeshRenderer>());
	}

    void OnTriggerEnter(Collider other) {
        Projectile projectile = other.gameObject.GetComponent<Projectile>();
        if (projectile != null) {
            Destroy(other.gameObject);
        }
    }
}
