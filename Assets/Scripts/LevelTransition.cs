﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LevelTransition : NetworkBehaviour {

    [SerializeField] public GameObject leftBorderObject;
    [SerializeField] public GameObject rightBorderObject;
}
