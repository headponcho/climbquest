﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Logger : MonoBehaviour {
    [SerializeField] string logString;

	// Update is called once per frame
	void Update () {
        Debug.Log(logString);
	}
}
