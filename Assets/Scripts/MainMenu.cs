﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    [SerializeField] GameObject mainMenuButtons;

    public void SoloGame(float timeBeforeLoad) {
        mainMenuButtons.SetActive(false);
        StartCoroutine(SpawnEagle(timeBeforeLoad));
        StartCoroutine(LoadSoloGame(timeBeforeLoad + 1.0f));
    }

    IEnumerator SpawnEagle(float timeBeforeLoad) {
        yield return new WaitForSeconds(timeBeforeLoad);
        FindObjectOfType<Nest>().SpawnEagle(FindObjectOfType<PlayerController>().gameObject);
    }

    IEnumerator LoadSoloGame(float timeBeforeLoad) {
        yield return new WaitForSeconds(timeBeforeLoad);
        SceneManager.LoadScene(1);
    }

    public void QuitGame() {
        Application.Quit();
    }
}
