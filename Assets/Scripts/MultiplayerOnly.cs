﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MultiplayerOnly : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (!NetworkClient.active) {
            gameObject.SetActive(false);
        }
    }
}
