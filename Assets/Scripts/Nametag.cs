﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Nametag : MonoBehaviour {

    [SerializeField] Text nameLabel;
	
	public void SetNameLabel(string name) {
        nameLabel.text = name;
    }
}
