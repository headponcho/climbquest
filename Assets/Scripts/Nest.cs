﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Nest : NetworkBehaviour {

    [SerializeField] float timeToTrigger = 3.0f;
    [SerializeField] Transform eagleSpawnPoint;
    [SerializeField] GameObject eaglePrefab;
    [SerializeField] public GameObject eagleDestination;
    [SerializeField] public LevelTransition returnTo;
    Dictionary<GameObject, float> playerToEnterTime = new Dictionary<GameObject, float>();
    List<GameObject> returningPlayers = new List<GameObject>();

    private void Update() {
        foreach (KeyValuePair<GameObject, float> entry in playerToEnterTime) {
            if (Time.time - entry.Value >= timeToTrigger) {
                SpawnEagle(entry.Key);
                playerToEnterTime.Remove(entry.Key);
                break;
            }
        }
    }

    private void OnTriggerEnter(Collider other) {
        PlayerController player = other.gameObject.GetComponent<PlayerController>();
        if (player != null && 
            !playerToEnterTime.ContainsKey(player.gameObject) && 
            !returningPlayers.Contains(player.gameObject)) {
            playerToEnterTime.Add(player.gameObject, Time.time);
        }
    }

    private void OnTriggerExit(Collider other) {
        playerToEnterTime.Remove(other.gameObject);
    }

    public void SpawnEagle(GameObject targetPlayer) {
        if (isServer || !ClimbQuestNetworkManager.singleton.isNetworkActive) {
            returningPlayers.Add(targetPlayer);

            GameObject eagle = Instantiate(eaglePrefab, eagleSpawnPoint.position, eagleSpawnPoint.rotation);

            if (NetworkServer.active) {
                NetworkServer.Spawn(eagle);
                eagle.GetComponent<Eagle>().RpcSetOrigin(gameObject.GetComponent<NetworkIdentity>().netId);
                eagle.GetComponent<Eagle>().RpcSetCurrentTarget(targetPlayer.GetComponent<NetworkIdentity>().netId);
            } else {
                eagle.GetComponent<Eagle>().SetOrigin(this);
                eagle.GetComponent<Eagle>().SetCurrentTarget(targetPlayer);
            }
        }
    }

    public void PlayerReturned(GameObject player) {
        if (returningPlayers.Contains(player)) {
            returningPlayers.Remove(player);
        }
    }
}
