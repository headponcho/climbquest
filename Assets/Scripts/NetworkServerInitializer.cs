﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkServerInitializer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if (!NetworkServer.active) {
            gameObject.SetActive(false);
        }
	}
}
