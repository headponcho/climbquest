﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : NetworkBehaviour {

    [Header("Gameplay")]
    [Tooltip("Determines how far from the ground the player can be before they are considered grounded")]
    [SerializeField] float groundedFudgeFactor = 0.1f;
    [SerializeField] float speed = 5.0f;
    [SerializeField] float jumpForce = 12.0f;

    [Header("Audio")]
    [SerializeField] AudioClip jumpSound;
    [SerializeField] AudioClip damageSound;
    AudioSource audioSource;

    [Header("Core")]
    [SerializeField] Collider standingCollider;
    [SerializeField] Collider crouchingCollider;
    [SerializeField] Transform mesh;

    [SyncVar] public string playerName;
    [SyncVar] public bool isCrouching;
    [SyncVar] public Vector3 moveDirection;

    float speedScale = 1.0f;

    Rigidbody rbody;
    Animator animator;

    bool isGrounded;
    Vector3 colliderExtents;

    PlayerState state;
    ChatClient chatClient;

    bool controlDisabled = false;

    public override void OnStartLocalPlayer() {
        playerName = FindObjectOfType<PlayerConfig>().playerName;
        LocalPlayerSetup();
        CmdUpdateName(playerName);
    }

    void Start() {
        rbody = GetComponent<Rigidbody>();
        animator = GetComponentInChildren<Animator>();
        colliderExtents = standingCollider.bounds.extents;
        state = new PlayerStateAirborne(this);
        state.OnStateEnter();

        if (!ClimbQuestNetworkManager.singleton.isNetworkActive) {
            LocalPlayerSetup();
        }
    }

    private void LocalPlayerSetup() {
        AttachCameraToPlayer();
        audioSource = GetComponent<AudioSource>();
        ChatClient[] chatClients = Resources.FindObjectsOfTypeAll<ChatClient>();
        if (chatClients.Length > 0) {
            chatClient = Resources.FindObjectsOfTypeAll<ChatClient>()[0];
        }
    }

    // Update is called once per frame
    void Update() {
        UpdateNametag();
        UpdateCrouchComponents();
        UpdateAnimatorParameters();
        UpdateMeshRotation();
        isGrounded = CalculateGroundedState();

        if (ClimbQuestNetworkManager.singleton.isNetworkActive && !isLocalPlayer) { return; }

        if (NetworkClient.active) {
            HandleChatInput();
        }

        if (!controlDisabled && !chatClient.IsChatInputEnabled()) {
            if (Debug.isDebugBuild) {
                HandleDebugInput();
            }
            state.StateUpdate();
        }
    }

    void FixedUpdate() {
        if (!isCrouching) {
            rbody.MovePosition(rbody.position + (moveDirection * speed * speedScale * Time.fixedDeltaTime));
        }
    }

    public void SetState(PlayerState newState) {
        if (state != null) {
            state.OnStateExit();
        }

        state = newState;

        if (state != null) {
            state.OnStateEnter();
        }
    }

    public void OnDamage() {
        if (audioSource != null) {
            audioSource.PlayOneShot(damageSound);
        }
    }

    public void SetPlayerControllable(bool isControllable) {
        controlDisabled = !isControllable;
        standingCollider.enabled = isControllable;
        crouchingCollider.enabled = isControllable;
        if (isLocalPlayer || !ClimbQuestNetworkManager.singleton.isNetworkActive) {
            CameraController cameraController = Camera.main.GetComponent<CameraController>();
            if (cameraController != null) {
                if (isControllable) {
                    cameraController.UnfreezeCamera();
                } else {
                    cameraController.FreezeCamera();
                }
            }
        }
    }

    public bool IsControlDisabled() {
        return controlDisabled;
    }

    void OnCollisionEnter(Collision collision) {
        Vector3 normal = collision.contacts[0].normal;
    }

    public bool IsGrounded() {
        return isGrounded;
    }

    public void UpdateMoveDirection() {
        moveDirection = new Vector3(CrossPlatformInputManager.GetAxis("Horizontal"), 0.0f, 0.0f);

        if (ClimbQuestNetworkManager.singleton.isNetworkActive && isLocalPlayer) {
            CmdUpdateMoveDirection(moveDirection.x);
        }
    }

    public void UpdateMeshRotation() {
        if (moveDirection.x != 0.0f) {
            mesh.rotation = Quaternion.Euler(0.0f, 90.0f * moveDirection.x, 0.0f);
        }
    }

    public void UpdateAnimatorParameters() {
        animator.SetFloat("MoveDirection", Mathf.Abs(moveDirection.x));
        animator.SetBool("IsGrounded", isGrounded);
        animator.SetBool("IsCrouching", isCrouching);
    }

    public void StopMovement() {
        moveDirection = Vector3.zero;
        rbody.velocity = Vector3.zero;
    }

    public void Jump() {
        rbody.AddForce(Vector3.up * jumpForce, ForceMode.VelocityChange);
        audioSource.PlayOneShot(jumpSound);
    }

    private void UpdateNametag() {
        Nametag nametag = GetComponent<Nametag>();
        if (nametag != null) {
            GetComponent<Nametag>().SetNameLabel(playerName);
        }
    }

    private void UpdateCrouchComponents() {
        standingCollider.enabled = !isCrouching;
        crouchingCollider.enabled = isCrouching;
    }

    private void AttachCameraToPlayer() {
        CameraController cameraController = Camera.main.GetComponent<CameraController>();
        if (cameraController != null) {
            cameraController.player = transform;
        }
    }

    private bool CalculateGroundedState() {
        // ignore the IgnoreRaycast (2) and Player (8) layers
        int layerMask = (1 << 2) | (1 << 8);
        layerMask = ~layerMask;

        // start the raycast just above the ground since gravity can cause the player's collider to be slightly in the ground for one frame
        float distanceFromGround = 0.3f;
        float halfHeight = colliderExtents.y - distanceFromGround;
        float halfWidth = colliderExtents.x;

        Vector3 bottomLeftPoint = transform.position + new Vector3(-halfWidth, -halfHeight, 0.0f);
        Vector3 bottomRightPoint = transform.position + new Vector3(halfWidth, -halfHeight, 0.0f);

        return Physics.Raycast(bottomLeftPoint, Vector3.down, distanceFromGround + groundedFudgeFactor, layerMask) || 
               Physics.Raycast(bottomRightPoint, Vector3.down, distanceFromGround + groundedFudgeFactor, layerMask);
    }

    private void HandleChatInput() {
        if (CrossPlatformInputManager.GetButtonDown("Chat")) {
            if (!chatClient.IsChatInputEnabled()) {
                chatClient.EnableChatInput();
            } else {
                chatClient.SendChatMessage();
            }
        }
    }

    private void HandleDebugInput() {
        if (CrossPlatformInputManager.GetButton("Debug.SpeedMod")) {
            speedScale = 5.0f;
        } else {
            speedScale = 1.0f;
        }
    }

    public void TransitionToLevel(LevelTransition levelTransition) {
        if (levelTransition != null) {
            if (isLocalPlayer || !ClimbQuestNetworkManager.singleton.isNetworkActive) {
                CameraController cameraController = Camera.main.GetComponent<CameraController>();
                cameraController.UpdateWorldBorders(levelTransition.leftBorderObject, levelTransition.rightBorderObject);
            }
            transform.SetPositionAndRotation(levelTransition.gameObject.transform.position,
                                             levelTransition.gameObject.transform.rotation);
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            moveDirection = Vector3.zero;
        }
    }

    [ClientRpc]
    public void RpcTransitionToLevel(NetworkInstanceId levelTransitionId) {
        TransitionToLevel(ClientScene.FindLocalObject(levelTransitionId).GetComponent<LevelTransition>());
    }

    [ClientRpc]
    public void RpcInitialLevelTransition() {
        TransitionToLevel(FindObjectOfType<NetworkStartPosition>().GetComponent<LevelTransition>());
    }

    [Command]
    public void CmdUpdateName(string name) {
        playerName = name;
        // Send a "connected" message here since the player is only allowed to set their name on server join
        FindObjectOfType<ChatServer>().SendPlayerConnectedMessage(playerName);
    }

    [Command]
    public void CmdUpdateCrouchState(bool crouchState) {
        isCrouching = crouchState;
    }

    [Command]
    public void CmdUpdateMoveDirection(float moveDirection) {
        this.moveDirection = new Vector3(moveDirection, 0.0f, 0.0f);
    }
}
