﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerState {
    protected PlayerController player;

    public virtual void OnStateEnter() { }
    public virtual void OnStateExit() { }

    public abstract void StateUpdate();

    public PlayerState(PlayerController player) {
        this.player = player;
    }
}

