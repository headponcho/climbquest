﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerStateAirborne : PlayerState {

    public PlayerStateAirborne(PlayerController player) : base(player) {}

    public override void OnStateEnter() {
        player.UpdateMoveDirection();

        // Add a negligible amount of y-velocity to prevent the state from changing 
        // when a jump is triggered and velocity is still zero. 
        Vector3 playerVelocity = player.GetComponent<Rigidbody>().velocity;
        player.GetComponent<Rigidbody>().velocity = new Vector3(playerVelocity.x, playerVelocity.y + float.Epsilon, playerVelocity.z);
    }

    public override void StateUpdate() {
        if (player.IsGrounded() && player.GetComponent<Rigidbody>().velocity.y <= 0.0f) {
            if (CrossPlatformInputManager.GetButton("Crouch")) {
                player.SetState(new PlayerStateCrouch(player));
            } else {
                player.SetState(new PlayerStateStand(player));
            }
        }
    }
}