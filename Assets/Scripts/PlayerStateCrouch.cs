﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerStateCrouch : PlayerState {

    public PlayerStateCrouch(PlayerController player) : base(player) {}

    public override void OnStateEnter() {
        player.isCrouching = true;

        if (ClimbQuestNetworkManager.singleton.isNetworkActive) {
            player.CmdUpdateCrouchState(true);
        }        
    }

    public override void OnStateExit() {
        player.isCrouching = false;

        if (ClimbQuestNetworkManager.singleton.isNetworkActive) {
            player.CmdUpdateCrouchState(false);
        }
    }

    public override void StateUpdate() {
        PlayerState newState = null;
        if (CrossPlatformInputManager.GetButtonDown("Jump")) {
            player.Jump();
            newState = new PlayerStateAirborne(player);
        } else if (!player.IsGrounded()) {
            newState = new PlayerStateAirborne(player);
        } else if (!CrossPlatformInputManager.GetButton("Crouch")) {
            newState = new PlayerStateStand(player);
        }

        if (newState != null) {
            player.SetState(newState);
        }
    }
}