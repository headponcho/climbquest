﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerStateStand : PlayerState {

    public PlayerStateStand(PlayerController player) : base(player) {}

    public override void OnStateEnter() {
    }

    public override void StateUpdate() {
        PlayerState newState = null;

        player.UpdateMoveDirection();

        if (CrossPlatformInputManager.GetButtonDown("Jump")) {
            player.Jump();
            newState = new PlayerStateAirborne(player);
        } else if (!player.IsGrounded()) {
            newState = new PlayerStateAirborne(player);
        } else if (CrossPlatformInputManager.GetButton("Crouch")) {
            newState = new PlayerStateCrouch(player);
        }

        if (newState != null) {
            player.SetState(newState);
        }
    }
}