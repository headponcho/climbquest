﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    [SerializeField] float speed = 20.0f;

    public void SetDirection(Vector3 direction) {
        Vector3 velocity = new Vector3(direction.x, direction.y, direction.z);
        velocity = velocity.normalized;
        velocity *= speed;
        gameObject.GetComponent<Rigidbody>().velocity = velocity;
        gameObject.transform.forward = velocity.normalized;
    }
}
