﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ProjectileSpawner : NetworkBehaviour {

    [Tooltip("Time to wait before starting the spawn loop (in seconds). " + 
             "Used to create spawn offsets for projectile patterns. ")]
    [SerializeField] float startDelay = 0.0f;
    [SerializeField] GameObject projectilePrefab;
    [SerializeField] Vector3 direction;
    [SerializeField] float projectileForce = 20.0f;

    [Tooltip("Time before the projectile destroys itself (in seconds)")]
    [SerializeField] float projectileLifetime = 5.0f;

    [Tooltip("Time between projectile spawns (in seconds)")]
    [SerializeField] float spawnCooldown = 0.5f;

    [SerializeField] bool syncWithAnimation = true;

    float elapsedTime = 0.0f;
    float activeTime = 0.0f;

    Animator animator;

    void Start() {
        activeTime = Time.time;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update () {
        bool offline = !ClimbQuestNetworkManager.singleton.isNetworkActive;
        if (isServer || offline) {
            if (Time.time < startDelay + activeTime) { return; }

            elapsedTime += Time.deltaTime;

            if (elapsedTime >= spawnCooldown) {
                elapsedTime -= spawnCooldown;
                if (syncWithAnimation) {
                    if (offline) {
                        PlayAttackAnimation();
                    } else {
                        RpcPlayAttackAnimation();
                    }
                } else {
                    FireProjectile();
                }
            }
        }
    }

    public void FireProjectile() {
        if (isServer || !ClimbQuestNetworkManager.singleton.isNetworkActive) {
            GameObject projectile = Instantiate(projectilePrefab, transform.position, transform.rotation);
            projectile.GetComponent<Projectile>().SetDirection(direction);
            projectile.GetComponent<KnockbackForce>().SetForce(projectileForce);

            if (NetworkServer.active) {
                NetworkServer.Spawn(projectile);
            }

            Destroy(projectile, projectileLifetime);
        }
    }

    [ClientRpc]
    public void RpcPlayAttackAnimation() {
        PlayAttackAnimation();
    }

    public void PlayAttackAnimation() {
        animator.SetTrigger("Attack");
    }
}
