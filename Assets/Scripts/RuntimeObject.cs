﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuntimeObject : MonoBehaviour {

	void Start () {
        if (Debug.isDebugBuild) {
            gameObject.transform.SetParent(FindObjectOfType<RuntimeObjectContainer>().transform);
        }
    }
}
