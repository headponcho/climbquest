﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuntimeObjectContainer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if (!Debug.isDebugBuild) {
            gameObject.SetActive(false);
        }
	}
}
