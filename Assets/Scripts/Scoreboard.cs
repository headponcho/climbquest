﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class Scoreboard : MonoBehaviour {

    [SerializeField] GameObject scoreboardPanel;
    [SerializeField] Text playerListText;

    private void Start() {
        scoreboardPanel.SetActive(false);
    }

    private void OnEnable() {
        UpdatePlayerListText();
    }

    private void Update() {
        if (!scoreboardPanel.activeSelf && CrossPlatformInputManager.GetButton("Player List")) {
            scoreboardPanel.SetActive(true);
        } else if (scoreboardPanel.activeSelf && !CrossPlatformInputManager.GetButton("Player List")){
            scoreboardPanel.SetActive(false);
        }

        if (scoreboardPanel.activeSelf) {
            UpdatePlayerListText();
        }
    }

    public void UpdatePlayerListText() {
        playerListText.text = "";
        foreach (PlayerController player in FindObjectsOfType<PlayerController>()) {
            playerListText.text += player.playerName + "\n";
        }
    }
}
