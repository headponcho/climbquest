﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ShadowCastLimiter : MonoBehaviour {

    [SerializeField] Transform level;

	// Use this for initialization
	void Start () {
		foreach (Transform objectSet in level) {
            foreach (Transform levelObject in objectSet) {
                if (levelObject.position.y > transform.position.y) {
                    foreach (MeshRenderer meshRenderer in levelObject.GetComponentsInChildren<MeshRenderer>()) {
                        meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                    }
                } else {
                    foreach (MeshRenderer meshRenderer in levelObject.GetComponentsInChildren<MeshRenderer>()) {
                        meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
                    }
                }
            }
        }
	}
}
