﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloModeInitializer : MonoBehaviour {

    void Awake() {
        if (ClimbQuestNetworkManager.singleton == null) { return; }
        if (ClimbQuestNetworkManager.singleton.isNetworkActive) {
            gameObject.SetActive(false);
        }
    }
}
