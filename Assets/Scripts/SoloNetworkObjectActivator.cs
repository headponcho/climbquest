﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SoloNetworkObjectActivator : MonoBehaviour {

    void Start () {
        // Re-activate objects that are automatically de-activated by the NetworkManager because the network was not active
        foreach (NetworkIdentity networkIdentity in Resources.FindObjectsOfTypeAll<NetworkIdentity>()) {
            GameObject networkObject = networkIdentity.gameObject;
            if (!networkObject.activeSelf) {
                networkObject.SetActive(true);
            }
        }
	}
}
