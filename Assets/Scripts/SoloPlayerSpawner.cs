﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SoloPlayerSpawner : MonoBehaviour {

    [SerializeField] GameObject playerPrefab;
    [SerializeField] LevelTransition startLevel;

    void Start() {
        SpawnPlayer();
    }

    public void SpawnPlayer() {
        GameObject player = Instantiate(playerPrefab, startLevel.transform.position, startLevel.transform.rotation);
        player.GetComponentInChildren<Canvas>().gameObject.SetActive(false);
        player.GetComponent<PlayerController>().TransitionToLevel(startLevel);
    }
}
