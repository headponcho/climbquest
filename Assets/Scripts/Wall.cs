﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour {

    void OnCollisionEnter(Collision collision) {
        PlayerController player = collision.gameObject.GetComponent<PlayerController>();

        if (player != null) {
            if (!player.IsGrounded()) {
                Vector3 normal = collision.contacts[0].normal;
                Vector3 playerMoveDir = player.moveDirection;

                // positive = hitting left wall, negative = hitting right wall
                float horizContactDir = normal.x;
                // player is moving RIGHT and hits a wall on the LEFT
                bool leftWallCollision = playerMoveDir.x > 0.0f && horizContactDir > 0.0f;
                // player is moving LEFT and hits a wall on the RIGHT
                bool rightWallCollision = playerMoveDir.x < 0.0f && horizContactDir < 0.0f;

                if (leftWallCollision || rightWallCollision) {
                    player.StopMovement();
                }
            }
        }
    }
}
